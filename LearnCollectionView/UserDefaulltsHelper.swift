//
//  UserDefaulltsHelper.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 10/3/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

enum UserDefaultKeys: String {
    case groupNumber
    case managedAppConfigUdid
    case managedAppConfigAsset
    
    static var keyGroupNumber: String {
        return UserDefaultKeys.groupNumber.rawValue
    }
    
    static var keyManagedAppConfigUdid: String {
        return UserDefaultKeys.managedAppConfigUdid.rawValue
    }

    static var keyManagedAppConfigAsset: String {
        return UserDefaultKeys.managedAppConfigAsset.rawValue
    }
    
}


class UserDefaulltsHelper {
    
    
    static func writeGroupUserDefaults(withGroupNumber groupNumber: String = "9") {
        UserDefaults.standard.set(groupNumber, forKey: UserDefaultKeys.keyGroupNumber)
    }
    
    static func readGroupUserDefaults() -> String {
        guard let groupNumber = UserDefaults.standard.object(forKey: UserDefaultKeys.keyGroupNumber) as? String else  { fatalError("Error - Getting default value for UserDefault") }
        return groupNumber
    }
    
    static func removeUserDefaults() {
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.keyGroupNumber)
    }
 
    static func registerUserDefaults() {
        UserDefaults.standard.register(defaults: [ UserDefaultKeys.keyGroupNumber : "9"])
    }
    
    /// managed app config functions
    
    
    /// Read Functions
    
    static func readManagedAppConfigAsset() -> String {
        guard let asset = UserDefaults.standard.object(forKey: UserDefaultKeys.keyManagedAppConfigAsset) as? String else  { fatalError("Error - Getting default value for readManagedAppConfigAsset") }
        return asset
    }
    
    
    static func readmanagedAppConfigUdid() -> String {
        guard let udid = UserDefaults.standard.object(forKey: UserDefaultKeys.keyManagedAppConfigUdid) as? String else  { fatalError("Error - Getting default value for readmanagedAppConfigUdid") }
        return udid
    }
    

    
    static func writeManagedAppConfigAsset(managedAppConfigAsset asset: String ) {
        UserDefaults.standard.set(asset, forKey: UserDefaultKeys.keyManagedAppConfigAsset)
    }
    
    
    static func writeManagedAppConfigUdid(managedAppConfigUdid udid: String ) {
        UserDefaults.standard.set(udid, forKey: UserDefaultKeys.keyManagedAppConfigUdid)
    }
    

    
}
